# Fizz Buzz Application

This application is completely written in kotlin using MVVM architecture.

Architecture
-----------
![](media/architecture.png)

ScreenShot
-----------
![](media/screen1.png)
![](media/screen2.png)

Libraries
-----------
- Androidx
- RxJava2
- Koin
- Google Material Design
